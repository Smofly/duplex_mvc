<div class="row">
    <div class="col-lg-10">
        <?php
        foreach ($news_list as $key => $row) {
            /** Cutter news texten ned til 280 tegn * */
            $string = substr($row->news_text, 0, 420) . '...';
            /** Ændre datoen på nyheden  * */
            $date = date("d-m-Y", strtotime($row->news_date_added));
            ?>
            <div class="well">
                <div class="media">
                        <img class="media-object img-responsive pull-left" src="<?= base_url("public/img/news/$row->news_img_path") ?>">
                    <div class="media-body">
                        <h4 class="media-heading"><?= $row->news_headline ?></h4>
                        <p><?= $string ?></p>
                        <p><a href='<?= base_url("home/news/$row->news_id ") ?>'><button class="btn btn-primary  btn-sm pull-right">Læs Mere</button></a></p>
                        <ul class="list-inline list-unstyled">
                            <li><span><i class="glyphicon glyphicon-user"></i> By <a href="<?= site_url("user/profile/$row->user_id")?>">  <?= $row->user_username ?></a></span></li>
                            <li><span><i class="glyphicon glyphicon-calendar"></i> <?= $date ?></span></li>
                            <li>|</li>
                            <span><i class="glyphicon glyphicon-comment"></i> <?= $row->CommentCount ?>  comments</span>
                            <li>|</li>
                            <li>
                                <span class="glyphicon glyphicon-star"></span>
                                <span class="glyphicon glyphicon-star"></span>
                                <span class="glyphicon glyphicon-star"></span>
                                <span class="glyphicon glyphicon-star"></span>
                                <span class="glyphicon glyphicon-star-empty"></span>
                            </li>
                            <li>|</li>
                            <li>
                                <!-- Use Font Awesome http://fortawesome.github.io/Font-Awesome/ -->
                                <span><i class="fa fa-facebook-square"></i></span>
                                <span><i class="fa fa-twitter-square"></i></span>
                                <span><i class="fa fa-google-plus-square"></i></span>
                            </li>
                        </ul>

                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
    <div class="col-lg-2">

    </div>
</div>



