<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('user_model', '', TRUE);
    }

    public function dashboard() {
        echo 'hej';
    }

    public function Login() {
        $email = $this->input->post('email');
        $password = $this->input->post('password');

        $result = $this->user_model->login_check($email, $password);

        $this->output->set_content_type('application_json');

        if ($result) {
            $newdata = array(
                'user_id' => $result[0]['user_id'],
                'logged_in' => TRUE
            );
            $this->session->set_userdata($newdata);
            $this->output->set_output(json_encode(['result' => 1]));
            return false;
        }

        $this->output->set_output(json_encode(['result' => 0]));
    }

    public function Register() {
        $this->load->view('inc/header_view');
        $this->load->view('user/register_view');
        $this->load->view('inc/footer_view');
    }

    public function Register_user() {
        $this->output->set_content_type('application/json');


        $this->form_validation->set_rules('username', 'Username', 'required|trim|min_length[4]|max_length[16]|callback_username_check');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email|callback_email_check');
        $this->form_validation->set_rules('password', 'Password', 'required|min_length[4]|max_length[16]');
        $this->form_validation->set_rules('password_confirm', 'Password Confirmation', 'required|min_length[4]|max_length[16]|matches[password]');

        $this->form_validation->set_message('is_unique', "That Username/Email Already Exists.");

        if ($this->form_validation->run() == false) {
            $this->output->set_output(json_encode(['result' => 0, 'error' => $this->form_validation->error_array()]));
            return false;
        }


        $username = $this->input->post('username');
        $email = strtolower($this->input->post('email'));
        $password = $this->input->post('password');
        $password_confirm = $this->input->post('password_confirm');

        $user_info = array(
            'user_username' => "$username",
            'user_email' => "$email",
            'user_password' => hash('sha256', $password . SALT),
        );

        $user_id = $this->user_model->insert_user($user_info);

        if ($user_id) {
            $newdata = array(
                'user_id' => $user_id,
                'logged_in' => TRUE
            );
            $this->session->set_userdata($newdata);
            $this->output->set_output(json_encode(['result' => 1]));
            return false;
        }

        $this->output->set_output(json_encode(['result' => 0, 'error' => 'User not created.']));
    }

    public function profile($user_id = NULL) {
        if ($user_id == null) {
            redirect('/');
        }
        $data['user_info'] = $this->user_model->get_user($user_id);

        $this->load->view('inc/header_view');
        $this->load->view('user/profile_view', $data);
        $this->load->view('inc/footer_view');
    }

    public function username_check($username) {
        if ($this->user_model->check_username($username) == 0) {
            return true;
        } else {
            $this->form_validation->set_message('username_check', 'That user is already taken');
            return false;
        }
    }

    public function email_check($email) {
        if ($this->user_model->check_email($email) == 0) {
            return true;
        } else {
            $this->form_validation->set_message('email_check', 'That Email is already in use');
            return false;
        }
    }

    public function Logout() {
        $this->session->sess_destroy();
        redirect('/');
    }

}
