<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class news_model extends CI_Model {

    public function news_get($news_id = NULL) {

        $this->db->select('*,  count(news_comments_id) CommentCount');
        $this->db->from('news');
        $this->db->join('news_img', 'news_id = news_img_news_id', 'left');
        $this->db->join('news_comments', 'news_id = news_comments_news_id', 'left');
        $this->db->join('user', 'user_id = news_user_id', 'left');

        if ($news_id == NULL) {
            
        } else {
            $this->db->where("news_id = $news_id");
        }
        $this->db->group_by('news_id');
        $this->db->order_by('news_id DESC');
        $query = $this->db->get();

        return $query->result();
    }

    public function news_update() {
        
    }

    public function news_comment_insert($data) {

        $this->db->insert('news_comments', $data);
        return $this->db->insert_id();
    }

    public function news_delete() {
        
    }

    public function news_comments_get($news_id) {

        $this->db->select('*');
        $this->db->from('news_comments');
        $this->db->join('user', 'user_id = news_comments_user_id', 'left');
        $this->db->where("news_comments_news_id = $news_id");
        $this->db->order_by('news_comments_id DESC');
        $query = $this->db->get();

        return $query->result();
    }

}
