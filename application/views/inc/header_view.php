<!DOCTYPE html>
<html lang="dk">
    <head>
       <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" rel="stylesheet">
        <link rel="stylesheet" href="<?= base_url() ?>public/css/bootstrap.min.css" />
        <link rel="stylesheet" href="<?= base_url() ?>public/css/style.css" />

        <script src="<?= base_url() ?>public/js/jquery.min.js"></script>
        <script src="<?= base_url() ?>public/js/bootstrap.js"></script>
    </head>
    <body>
        <nav class="navbar navbar-default">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Duplex</a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="<?= base_url('home/news') ?>">News</a></li>
                        <li><a href="#">Forum</a></li>
                        <li><a href="#">Contact</a></li>
                    </ul>
                    <?php
                    if ($this->session->userdata('logged_in') == false) {
                        ?>
                        <form id="login_form" class="navbar-form navbar-right" role="form" action="<?= site_url('user/login') ?>" method="POST">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                <input id="email" type="email" class="form-control" name="email" value="" placeholder="Email Address">                                        
                            </div>

                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                                <input id="password" type="password" class="form-control" name="password" value="" placeholder="Password">                                        
                            </div>

                            <input type="submit" class="btn btn-primary" value="Login" />
                            <a href="<?= site_url('user/register') ?>" class="btn btn-primary">Register</a>

                        </form>
                        <?php
                    } else {
                        ?>
                        <ul class="nav navbar-nav navbar-right">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">User Control Panel <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="#">Profile</a></li>
                                    <li><a href="#">Edit Profile</a></li>
                                    <li><a href="#">Admin</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="<?= site_url('user/logout')?>">Logout</a></li>
                                </ul>
                            </li>
                        </ul>
                        <?php
                    }
                    ?>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>
        <div class="container">

            <script type="text/javascript">
                $(function () {

                    $("#login_form").submit(function (evt) {
                        evt.preventDefault();
                        var url = $(this).attr('action');
                        var postData = $(this).serialize();

                        $.post(url, postData, function (o) {
                            if (o.result === 1) {
                                window.location.href = '<?= site_url('user/dashboard') ?>';
                            } else {
                                alert('Invalid Login');
                            }
                        }, 'json');

                    });

                });
            </script>