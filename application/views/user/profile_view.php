<?php
if (empty($user_info)) {
    ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-danger">Den valgte profil findes ikke</div>
        </div>
    </div>
    <?php
}
foreach ($user_info as $key => $row) {
    ?>
    <div class="row">
        <div class="col-md-12">
            <div class="well profile">
                <div class="col-sm-12">
                    <div class="col-xs-12 col-sm-8">
                        <h2><?= $row['user_realname'] . ' ' . $row['user_realsurename'] ?></h2>
                        <p><strong>About: </strong> </p>
                        <p><strong>Hobbies: </strong>  </p>
                        <p><strong>Skills: </strong>
                            <span class="tags">html5</span> 
                            <span class="tags">css3</span>
                            <span class="tags">jquery</span>
                            <span class="tags">bootstrap3</span>
                        </p>
                    </div>             
                    <div class="col-xs-12 col-sm-4 text-center">
                        <figure>
                            <img src="http://www.localcrimenews.com/wp-content/uploads/2013/07/default-user-icon-profile.png" alt="" class="img-circle img-responsive center-block">
                            <figcaption class="ratings">
                                <p>Ratings
                                    <a href="#">
                                        <span class="fa fa-star"></span>
                                    </a>
                                    <a href="#">
                                        <span class="fa fa-star"></span>
                                    </a>
                                    <a href="#">
                                        <span class="fa fa-star"></span>
                                    </a>
                                    <a href="#">
                                        <span class="fa fa-star"></span>
                                    </a>
                                    <a href="#">
                                        <span class="fa fa-star-o"></span>
                                    </a> 
                                </p>
                            </figcaption>
                        </figure>
                    </div>
                </div>            
                <div class="col-xs-12 divider text-center">
                    <div class="col-xs-12 col-sm-4 emphasis">
                        <h2><strong> 20,7K </strong></h2>                    
                        <p><small>Followers</small></p>
                        <button class="btn btn-success btn-block"><span class="fa fa-plus-circle"></span> Follow </button>
                    </div>
                    <div class="col-xs-12 col-sm-4 emphasis">
                        <h2><strong>245</strong></h2>                    
                        <p><small>Following</small></p>
                        <button class="btn btn-info btn-block"><span class="fa fa-user"></span> View Profile </button>
                    </div>
                    <div class="col-xs-12 col-sm-4 emphasis">
                        <h2><strong>43</strong></h2>                    
                        <p><small>Snippets</small></p>
                        <div class="btn-group dropup btn-block col-sm-4">
                            <button type="button" class="btn btn-primary button_text"><span class="fa fa-gear"></span> User Options </button>
                            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                                <span class="caret"></span>
                                <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <ul class="dropdown-menu text-left" role="menu">
                                <li><a href="#"><span class="fa fa-envelope pull-right"></span> Send an email </a></li>
                                <li><a href="#"><span class="fa fa-list pull-right"></span> Add or remove from a list  </a></li>
                                <li class="divider"></li>
                                <li><a href="#"><span class="fa fa-warning pull-right"></span>Report this user for spam</a></li>
                                <li class="divider"></li>
                                <li><a href="#" class="btn disabled" role="button"> Unfollow </a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>                 
        </div>
    </div>
<?php } ?>