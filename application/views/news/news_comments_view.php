<div class="row">
    <div class="col-lg-12">
        <?php
        foreach ($news_read_more as $key => $row) {
            /** Ændre datoen på nyheden  * */
            $date = date("d-m-Y", strtotime($row->news_date_added));
            ?>
            <div class="well">
                <div class="media">
                    <a class="pull-left" href="#">
                        <img class="media-object" src="<?= base_url("public/img/news/$row->news_img_path") ?>">
                    </a>
                    <div class="media-body">
                        <h4 class="media-heading"><?= $row->news_headline ?></h4>
                        <p><?= $row->news_text ?></p>
                        <ul class="list-inline list-unstyled">
                            <li><span><i class="glyphicon glyphicon-user"></i> By <?= $row->user_username ?></span></li>
                            <li><span><i class="glyphicon glyphicon-calendar"></i> <?= $date ?></span></li>
                            <li>|</li>
                            <span><i class="glyphicon glyphicon-comment"></i> <?= $row->CommentCount ?>  comments</span>
                            <li>|</li>
                            <li>
                                <span class="glyphicon glyphicon-star"></span>
                                <span class="glyphicon glyphicon-star"></span>
                                <span class="glyphicon glyphicon-star"></span>
                                <span class="glyphicon glyphicon-star"></span>
                                <span class="glyphicon glyphicon-star-empty"></span>
                            </li>
                            <li>|</li>
                            <li>
                                <!-- Use Font Awesome http://fortawesome.github.io/Font-Awesome/ -->
                                <span><i class="fa fa-facebook-square"></i></span>
                                <span><i class="fa fa-twitter-square"></i></span>
                                <span><i class="fa fa-google-plus-square"></i></span>
                            </li>
                        </ul>

                    </div>
                </div>
            </div>

        </div>
        <div class="col-md-12">
            <div class="blog-comment">
                <?php
                if ($this->session->flashdata('sucess_besked') == true) {
                    ?>
                    <div id="register_form_sucess" class="alert alert-success" role="alert"><?= $this->session->flashdata('sucess_besked') ?></div>
                    <?php
                }
                ?>

                <h3 class="text-success">Comments</h3>
                <hr/>
                <?php
                foreach ($news_comments as $row_comments) {
                    $comment_date = date("M j, Y H:i", strtotime($row_comments->news_comments_date_added));
                    ?>
                    <ul class="comments">
                        <li class="clearfix">
                            <img src="http://bootdey.com/img/Content/user_1.jpg" class="avatar" alt="">
                            <div class="post-comments">
                                <p class="meta"><?= $comment_date ?> <a href="#"><?= $row_comments->user_username ?></a> says : <i class="pull-right"><a href="#"><small>Reply</small></a></i></p>
                                <p><?= $row_comments->news_comments_text ?></p>
                            </div>
                        </li>
                    </ul>
                <?php } ?>
            </div>
        </div>
        <?php
        if ($this->session->userdata('logged_in') == true) {
            ?>
            <div class="col-md-12">
                <div id="register_form_error" class="alert alert-danger" role="alert"></div>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <form id="comment_form" accept-charset="UTF-8" action="<?= site_url("news/post_news_comment/$row->news_id") ?>" method="POST">
                            <div class="form-group">
                                <textarea class="form-control" id="area" name="news_comment" placeholder="Type in your message" rows="5"></textarea>
                                <h6 class="pull-right"> (<span id="maxlength">320</span> characters left)</h6>
                            </div>
                            <input type="submit" class="btn btn-info"  value="Post New Message" />
                        </form>
                    </div>
                </div>
            </div>
        <?php }?>
        </div>


        <script>
            $(function () {
                $("#register_form_error").hide();


                $("#comment_form").submit(function (evt) {
                    evt.preventDefault();
                    var url = $(this).attr('action');
                    var postData = $(this).serialize();

                    $.post(url, postData, function (o) {
                        if (o.result == 1) {
                            window.location.href = '<?= base_url("home/news/$row->news_id") ?>';
                        } else {
                            $("#register_form_error").show();
                            var output = '<ul>';
                            for (var key in o.error) {
                                var value = o.error[key];
                                output += '<li>' + value + '</li>';
                            }
                            output += '</ul>';
                            $("#register_form_error").html(output);
                        }
                    }, 'json');

                });

            });
        </script>

    <?php } ?>