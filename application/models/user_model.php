<?php

class User_model extends CI_Model {

    // ------------------------------------------------------------------------
    public function __construct() {
        parent::__construct();
    }

    public function login_check($email, $password) {

        $password = hash('sha256', $password . SALT);

        $this->db->select('*');
        $this->db->from('user');
        $this->db->where("user_email = '$email' and user_password = '$password'");

        $query = $this->db->get();

        return $query->result_array();
    }

    public function insert_user($data) {

        $this->db->insert('user', $data);
        return $this->db->insert_id();
    }

    public function check_username($username) {
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where("user_username = '$username'");

        $query = $this->db->get();

        return $query->num_rows();
    }

    public function get_user($user_id) {

        $this->db->select('*');
        $this->db->from('user');
        $this->db->where("user_id = '$user_id'");

        $query = $this->db->get();

        return $query->result_array();
    }

    public function check_email($email) {
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where("user_email = '$email'");

        $query = $this->db->get();

        return $query->num_rows();
    }

    // ------------------------------------------------------------------------
}
