<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class News extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model('news_model', '', TRUE);
    }

    public function index() {
        
    }

    public function post_news_comment($news_id) {
        if (!is_numeric($news_id)) {
            redirect('home/news');
        }
        $this->output->set_content_type('application/json');


        $this->form_validation->set_rules('news_comment', 'Comment', 'required|trim|max_length[320]');

        if ($this->form_validation->run() == false) {
            $this->output->set_output(json_encode(['result' => 0, 'error' => $this->form_validation->error_array()]));
            return false;
        }

        $news_comments = $this->security->sanitize_filename($this->input->post('news_comment'));
        $news_comments_user_id = $this->session->userdata('user_id');
        $data = array(
            'news_comments_text' => "$news_comments",
            'news_comments_news_id' => "$news_id",
            'news_comments_user_id' => "$news_comments_user_id"
        );

        $insert = $this->news_model->news_comment_insert($data);

        if ($insert) {
            $this->session->set_flashdata('sucess_besked', 'Besked Modtaget');
            $this->output->set_output(json_encode(['result' => 1]));
            return false;
        }

        $this->output->set_output(json_encode(['result' => 0, 'error' => 'Message not created.']));
    }

}
