<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model('news_model', '', TRUE);
    }

    public function index() {
        $this->news();
    }

    public function news($news_id = NULL) {
        if ($news_id == NULL || !is_numeric($news_id)) {
            $data['news_list'] = $this->news_model->news_get();

            $this->load->view('inc/header_view');
            $this->load->view('news/news_view', $data);
            $this->load->view('inc/footer_view');
        } else {
                   
            $data['news_read_more'] = $this->news_model->news_get($news_id);
            $data['news_comments'] = $this->news_model->news_comments_get($news_id);

            $this->load->view('inc/header_view');
            $this->load->view('news/news_comments_view', $data);
            $this->load->view('inc/footer_view');
        }
    }

}
